// Objetcs - real world object with its own characteristics
// organize context through key-value
// {} object literals
// keys provide lable
// key: value (together are called properties)

let dog = ["loyal", 4, "tail", "Husky"];

let dog2 = {
    breed: "Husky",
    color: "White",
    legs: 4,
    isHuggable: true, 
    isLoyal: true
};

/* Mini Activity */

let favoriteMovie = {
    title: "WandaVision",
    publisher: "Marvel",
    year: 2021,
    director: "Matt Shakman",
    isAvailable: true
};

console.log(favoriteMovie);

// accessing object properties = objectName.propertyName

console.log(favoriteMovie.title);
console.log(favoriteMovie.publisher);

favoriteMovie.title = "WandaVision: 2";
console.log(favoriteMovie.title);

/* Mini Activity */

favoriteMovie.title = "Ang Probinsyano";
favoriteMovie.publisher = "FPJ Productions";
favoriteMovie.year = "1972";

console.log(favoriteMovie);

let course = {
    title: "Philosophy 101",
    description: "Learn the values of life",
    price: 5000,
    isActive: true,
    instructors: ["Mr. Johnson", "Mrs. Smith", "Mr. Francis"]
};

console.log(course);
console.log(course.instructors);
console.log(course.instructors[1]);
course.instructors.pop();
console.log(course.instructors);

// always access onjectName.propertyNameArr.method()

/* Mini Activity */

course.instructors.push("Mr. Mcgee");
console.log(course.instructors);

console.log(course.instructors.includes("Mr. Johnson"));

// function to add new instructors

function addNewInstructor(newInstructor) {

    // if (course.instructors.indexOf(newInstructor) > 0)
    if (course.instructors.includes(newInstructor)) {
        console.log("Instructor already added.");
    } 

    else {
        course.instructors.push(newInstructor);
        console.log("Thank you. Instructor added");
    }
}

addNewInstructor("Mr. Marco");
addNewInstructor("Mr. Smith");
addNewInstructor("Mr. McCall");
addNewInstructor("Mr. McCall");
console.log(course.instructors);

// create/initialize an empty obj and then add its properties after

let instructor = {};
console.log(instructor);

instructor.name = "James";
console.log(instructor);

/* Mini Act */

instructor.age = 56,
instructor.gender = "Male",
instructor.department = "Humanities",
instructor.salary = 50000,
instructor.subjects = ["Philosophy", "Humanities", "Logic"];

console.log(instructor);

instructor.address = {
    street: "#1 Maginhawa St.",
    city: "Quezon City",
    country: "Philippines"
}

console.log(instructor);

// access value from nested object
console.log(instructor.address.street); 

// Create Objects using a constructor function
// create reusable function to create objects whose structure and keys are the same
// Like a blueprint

function superHero(name, superpower, powerLevel) {
    // "this" is when added in a constructor functions refers to the object that will be made by the function
    /*
        {
            name: <valueOfParameterNAme>
            superPower: <valueOfParameterSuperpower>
            powerLevel: <valueOfParameterPowerLevel>
        }
    */

        this.name = name;
        this.superpower = superpower;
        this.powerLevel = powerLevel;
}

// Create an object out of our Superhero constructor function
// "new" keyword is added to allow us to create a new object out of our function

let superhero1 = new superHero("Saitama", "One Punch", 30000);
console.log(superhero1);

// this is self-referentials applied in each object
function laptopDetails(name, brand, price) {
    this.name = name;
    this.brand = brand;
    this.price = price;
}

let laptop1 = new laptopDetails("Inspiron 15 5000", "Dell", 4500);
console.log(laptop1);
let laptop2 = new laptopDetails("Aspire 15", "Acer", 35000);
console.log(laptop2);

// Object methods are function associated with an object
// function is a property of an object and function belongs to the object
// methods are tasks that an object can perform or do


let person = {
    name: "Slim Shady",
    talk: function() {
        // methods are functions associated as property of an object
        console.log("Hi! My name is, What? My name is who? " + this.name);
        // console.log(this);
        // this inside object
        // this refers to the object where method is assoc
    }
}

let person2 = {
    name: "Dr. Dre",
    greet: function(friend) {
        console.log("Good day, " + friend.name);
    }
}

person.talk();
person2.greet(person);

// create a constructor with a built-in method

function Dog(name, breed) {
    this.name = name,
    this.breed = breed;
    this.greet = function(friend) {
        console.log("Bark! bark, " + friend.name)
    }
}

let dog1 = new Dog("Rocky", "Bulldog");
console.log(dog1);

dog1.greet(person);
dog1.greet(person2);

let dog3 = new Dog("Blackie", "Rottweiler");
console.log(dog3);