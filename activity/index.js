let trainer = {
    name: "Ash Ketchum",
    age: "10",
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: ["Brock", "Misty"],
    // friends: {
    //     friendName: ["Brock", "Misty"]
    // }
    talk: function() {
        console.log("Pikachu! I choose you");
    }
}

console.log(trainer);

// dot notation result
console.log("Result of dot notation:")
console.log(trainer.name);

// bracket notation result
console.log("Result of square bracket notation:")
console.log(trainer.pokemon);

// talk method result
console.log("Result of talk method:");
console.log(trainer.talk());

function pokemonDetails(name, level, health, attack) {
    this.name = name;
    this.level = level;
    this.health = level * 3;
    this.attack = level * 1.5;

    this.tackle = function(pokemon) {
        console.log(this.name + " tackled " + pokemon.name);

        let damageReceived = pokemon.health - this.attack;
        console.log(pokemon.name + "'s health is reduced to " + damageReceived);
        
        pokemon.health = damageReceived;

        if (damageReceived <= 0) {
            // this.faint();
        }
    };

    this.faint = function() {
        console.log(name + " has fainted!");
    };
}

let pokemonOne = new pokemonDetails("Pikachu", 12, 24, 12);
console.log(pokemonOne); 
let pokemonTwo = new pokemonDetails("Geodude", 8, 16, 8);
console.log(pokemonTwo);
let pokemonThree = new pokemonDetails("Mewtwo", 100, 200, 100);
console.log(pokemonThree);

pokemonThree.tackle(pokemonOne);
pokemonOne.faint();

console.log(pokemonOne);
